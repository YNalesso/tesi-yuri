
\chapter{Reti Neurali}
\label{chap:neuralnet}
\vspace{0.8cm}
Nel 1950 Alan Turing pose nel suo articolo scientifico \textit{Computing Machinery and Intelligence} la domanda "\textit{Are there imaginable digital computers which would do well in the imitation game?}" \footfullcite{Touring:int}. Chiese cioè se degli elaboratori digitali fossero in grado di emulare l’uomo tanto da non permettere ad un terzo di distinguerli, come descritto nel suo \textit{Imitation Game} (\ref{app:imitation}). Solo due anni dopo Arthur Samuel scrisse il primo programma che imparava da un insieme di dati di training il gioco della dama; esso fu implementato tramite un elaboratore IBM ed aggiungeva al suo set di mosse quelle vincenti che via via vedeva applicare fino a riuscire a battere anche i migliori giocatori. Nel 1958 Frank Rosenblatt ideò la prima rappresentazione matematica dell’unità della rete neurale, il percettrone\footfullcite{Rosenblatt:perceptron} (\ref{ssec:perceptron}), la loro combinazione è possibile considerarla come il più semplice modello di rete neurale \textit{feed-forward} (\ref{sec:nn f-f}). Nel percettrone lo stadio di input è direttamente collegato a quello di output attraverso una funzione a gradino, senza transizioni intermedie come invece avviene attualmente (\ref{sec:activation}), attraverso dei collegamenti pesati. 

A questo punto l'elaboratore ha sia un unità logica con cui pensare sia un metodo con cui imparare e, grazie ai progressi in termini di risorse computazionali e di memoria degli ultimi anni,  da questi strumenti di base si sono sviluppate le odierne reti neurali e i recenti algoritmi di \textit{machine} (\ref{sec:mlearning}) e \textit{deep learning} (\ref{ssec:dlearning}). 

I risultati di questa continua ricerca sono stati evidenziati con il progetto ImageNet, un enorme database di immagini utilizzato per il riconoscimento degli oggetti. Questo progetto, patrocinato dall'università di Standford, organizza annualmente la gara LSVRC (\textit{Large Scale Visual Recognition Challenge}) per valutare i migliori algoritmi per il riconoscimento delle immagini. La conferma dei recenti miglioramenti nel riconoscimento e nella classificazione automatica delle immagini è stata il superamento, nel 2015, delle performance di un operatore umano nel riconoscimento degli oggetti\footfullcite{ILSVRC15:art}.

Una rete neurale artificiale dunque, è un modello di elaborazione delle informazioni ispirato a quello utilizzato dai sistemi nervosi biologici, come il cervello. Questo sistema è composto da molte unità di elaborazione connesse in rete fra loro, chiamate neuroni, e opportunamente allenate allo scopo di realizzare un task molto più complesso. Le reti vengono configurate attraverso un processo di apprendimento, il quale avviene elaborando una moltitudine di dati già catalogati, per compiere specifiche applicazioni come il riconoscimento di pattern o caratteristiche peculiari.

\section{Neuroni artificiali}
Le unità fondamentali della rete neurali sono, come nel cervello umano, i neuroni; essi compiono micro-operazioni che sommate assieme servono ad eseguire compiti più difficili.

\subsection{Percettrone} \label{ssec:perceptron}
Il percettrone, sviluppato da Frank Rosenblatt, è stato uno dei primi modelli di neurone artificiale proposti; esso produce un singolo output da diversi input $x_i$.

\begin{figure}[h]
	\centering
	\includegraphics[width=5cm]{figure/tikz0}
	\caption{Schema percettrone}
	\label{fig:tikz0}
\end{figure}

Quest'ultimi vengono combinati linearmente associando un peso ad ognuno, il risultato di questa operazione viene confrontato con un valore di soglia, se maggiore, l’output del percettrone è 1, altrimenti è 0.
\begin{center}
	$output=
	\begin{cases}
	0, & \mbox{se }\sum_{j}w_{j}x_{j}\leq\mbox{soglia} \\
	1, &\mbox{se } \sum_{j}w_{j}x_{j}\geq\mbox{soglia}  
	\end{cases} $
\end{center}

 Definiamo quindi $\mathbf{x}$ come il vettore di input, $\mathbf{w}$ quello dei pesi e il bias, il cui valore è l’opposto del valore di soglia. Possiamo ora definire l’output come:
 
\begin{center}
$output=
\begin{cases}
	0, & \mbox{se } \mathbf{w}\cdot \mathbf{x}^T+b\leq0 \\
	1, & \mbox{se } \mathbf{w}\cdot \mathbf{x}^T+b\geq0 
\end{cases} $
\end{center}

Un singolo percettrone può compiere decisioni molto basilari come può essere ad esempio l'implementazione di una NAND (\ref{app:perNAND}). Tuttavia, combinandoli assieme secondo un'opportuno schema di rete e impostando dei pesi adeguati, è possibile realizzare delle funzioni molto complesse.
\begin{figure}[h]
	\centering
	\includegraphics[width=10cm]{figure/tikz1}
	\caption[Rete di percettroni]{Rete di percettroni}
	\label{fig:tikz1}
\end{figure}

In questa rete, la prima colonna di percettroni, chiamata primo layer, compie delle operazioni piuttosto semplici pesando i valori di input. I percettroni del secondo layer elabora i dati già in parte trasformati dal primo layer che, rispetto ai dati in input, presentano già un livello di astrazione maggiore in relazione al task che la rete deve implementare. Allo stessa maniera si può pensare al terzo ed ultimo layer; una rete con layer multipli può così effettuare classificazioni molto complesse.

\subsection{Funzione di attivazione} \label{sec:activation}
Il problema principale di una rete di percettroni è l'elevata variazione dell'output da 0 a 1 anche con una piccola fluttuazione nei dati di input causata da una componente di errore. Questo avviene perchè vi è una funzione a gradino, che compara il valore di output con quello di soglia, con codominio $[0\mbox{ }1] $. Per ovviare a questo problema si è introdotta la funzione di attivazione $\phi$: essa ha come argomento la combinazione lineare degli input con i pesi a cui va sommato il bias.\\ Degli esempi di funzione possono essere:

\begin{figure}[h]
	\begin{subfigure}{.5\textwidth}
	\centering
	\includegraphics[width=7cm]{figure/sigmoid}
	\caption*{Sigmoide} 
	\label{fig:sig}
	\end{subfigure}
	\begin{subfigure}{.5\textwidth}
	\centering
	\includegraphics[width=7cm]{figure/tanh} 
	\caption*{Tangente iperbolica}
	\label{fig:tanh}
	\end{subfigure}
	\caption{Funzioni di attivazione}%
	\label{fig:activationFunction}%
\end{figure} 


La funzione sigmoide $\sigma(x)\equiv\frac{1}{1+\exp^{-x}}$ è stata molto utilizzata e rappresenta idealmente uno scalino “smussato”. Nelle reti neurali più recenti, tuttavia, è stata rimpiazzata dalla funzione $\tanh$,che corrisponde alla traslazione della funzione sigmoidale, $\tanh(x)=2\sigma(2x)-1$. Si risolve così il problema dei valori di uscita non centrati in 0. %\hl{\textbf{\textit{ma non quello di azzerare il gradiente ad avvenuta saturazione.} }}

Una funzione che negli ultimi anni è divenuta popolare è la ReLU, dall'inglese \textit{Rectified Linear Unit}, con funzione $f(x)=\max(0,x) $ per la sua semplicità di calcolo e la capacità di velocizzare il processo di apprendimento della rete.
\section{Layer} \label{sec:label}
\begin{minipage}{0.45\textwidth}
	\centering	
	\includegraphics[width=5cm]{figure/ANN.png}
	\captionof{figure}{Grafo di una rete neurale}
\end{minipage}
\hfill
\begin{minipage}{0.55\textwidth}
	Le reti neurali più comuni sono composte da tre gruppi o layer:
	\begin{itemize}
		\item Layer di input
		\item Layer nascosto
		\item Layer di output
	\end{itemize}
\end{minipage}
\medskip 

Il primo è connesso al secondo che è connesso col terzo. Il layer di input rappresenta l’informazione grezza che viene inviata alla rete, l’attività di ogni neurone del layer nascosto è determinata da quella del layer di input e dal peso di ogni interconnessione. Il layer intermedio può essere composto da uno o più layer e deriva il suo nome dal fatto che non abbiamo una rappresentazione di esso ma percepiamo il funzionamento da quello di output.il funzionamento di quest'ultimo dipende infatti dall’attività di quello precedente e alle interconnessioni con esso.

\section{Tipi di Rete}
I tipi di rete vengono definiti dalla loro descrizione topologica mediante un grafo pesato dove i nodi sono i neuroni e i rami sono le interconnessioni tra essi

\subsection{Feed-Forward} \label{sec:nn f-f}
In queste reti il segnale può viaggiare in una sola direzione quindi l’output di ogni layer non ha effetto sul layer stesso (non vi è la presenza di anelli nel grafo di rappresentazione). La rete è estensivamente utilizzata per il riconoscimento di pattern, in questa specifica applicazione durante la fase di addestramento viene allenata ad associare un modello di uscita ai dati in ingresso. Questo modello è associato con un grafo aciclico diretto: ad ogni ramo è associata una funzione e un percorso da un nodo ad un altro costituisce una composizione di funzioni differenti. Ad esempio potremmo avere tre funzioni $ g,h \mbox{ e } j $ connesse in una catena a formare $f(x)=g(h(j(x)))$; queste strutture a catena sono quelle più usate per descrivere le reti neurali. In questo caso $j$ è chiamato il primo layer, $h$ il secondo e così via. La lunghezza complessiva della catena ci dà la profondità del modello, profondità che dà il nome a \emph{Deep Learning}. 

\subsection{Feed-Back}
Al contrario delle reti precedenti le reti \textit{feed-back} permettono al segnale di viaggiare in entrambe le direzioni introducendo loop nel grafo.Questa topologia è attualmente in studio e sembra promettente in quanto a velocità nell'ottenere risultati soprattuto nella classificazione. 



\section{Rete Neurale Convoluzionale (CNN)}
Le reti convoluzionali sono un tipo specializzato di reti neurali ottimizzate per l’elaborazioni di dati che hanno una topologia a griglia, bidimensonale o monodimensionale, come le immagini o i vettori di dati. Il nome stesso suggerisce che esse sono composte da uno o più strati che utilizzano l’operazione matematica della convoluzione. Questo tipo di rete sarà quello utilizzato per creare la deep network atta al riconoscimento dei materiali poiché i dati con cui alleneremo la rete sono dei vettori monodimensionali.
\subsection{Layer tipico}
Le reti neurali convoluzionali usano tre idee alla base dei loro layer: \textit{local receptive field}, \textit{shared wheights} e quella del \textit{pooling}.
\begin{itemize}
	\item \textbf{\textit{Local receptive fields}}\\L’idea di base è quella di connettere una parte dei dati di input (ad esempio una regione di pixel o delle celle di un array) ad un singolo neurone, senza quindi effettuare una connessione completa tra il layer di input e quello successivo. Questo permette ad ogni neurone di imparare ad analizzare solo il suo particolare campo ricettivo. La diminuzione della quantità di neuroni nel layer e del numero di connessioni consente una riduzione dei pesi e dei bias da calcolare in fase di allenamento. 
	\item \textbf{\textit{Shared weights and bias}}\\Il bias e i pesi delle connessioni tra il neurone e gli input del singolo campo ricettivo sono condivisi tra tutti i neuroni. In questo modo ogni neurone identifica esattamente la stessa feature solamente in differenti posizioni dell’input. Perciò il layer di input esso viene collegato simultaneamente a molteplici layer nascosti per l’estrazione di altrettante caratteristiche. Il vantaggio è una riduzione drastica del numero di pesi da modificare durante l’apprendimento della rete\footnote{Completare con esempio numerico delle immagini?}. Il layer così generato è chiamato layer convoluzionale.
	\item \textbf{\textit{Pooling layer}}\\ Sono solitamente utilizzati dopo i layer convoluzionali e semplifica l’informazione in uscita da essi condensandone l’output riducendo ulteriormente il numero di neuroni dello strato successivo.
\end{itemize}
%\hl{\textbf{\textit{Completare con descrizione di un layer tipico:\\local receptive fields, shared weight and biases, pooling}}}

\section{Apprendimento}
La conoscenza della rete è contenuta nei pesi delle varie interconnessioni e nei bias dei singoli nodi, l’apprendimento è quindi  identificato come la modifica di questi valori in modo da migliorare la precisione dell’output. Possiamo distinguere tra una rete statica con quindi tutti i pesi fissati a priori e una rete adattiva che è capace di adattare i pesi. 

Per l’apprendimento viene definita una funzione di costo, il cui valore attraverso il processo di training dev’essere reso il più piccolo possibile. Questa funzione da una idea della correttezza dell’output rispetto al risultato aspettato, ad esempio può essere definita come lo scarto quadratico medio.

Durante l’apprendimento le reti neurali cercano di implementare una funzione  $f(x)$ in grado di approssimare una funzione incognita  $f^*(x)$: i dati di apprendimento ci forniscono, con il rumore, dei campioni della  $f^*$ da stimare in diversi punti. Ogni dato $x$ è accompagnato da una label $y\approx f(x)$. Le label specificano direttamente il comportamento del layer di output ad ogni punto $x$, infatti esso deve produrre un valore vicino ad $y$. Il comportamento degli altri layer non è direttamente specificato, per questo viene utilizzato l’algoritmo di apprendimento per decidere come essi vengono usati per produrre l’output desiderato e per meglio implementare una approssimazione di $f^*$. Siccome i dati di apprendimento non mostrano la configurazione per questi layer essi vengono chiamati layer nascosti.

In machine learning, i classificatori si dividono secondo tre grandi classi, a seconda del tipo di apprendimento (training) che viene effettuato. Similarmente, i pesi di ogni layer in una rete neurale possono essere modificati secondo diversi tipi di training. 
\begin{itemize}
	\item \textbf{Apprendimento supervisionato}, cioè è disponibile un dataset di dati classificati in cui ad ogni input corrisponde anche il dato di output correttamente generato. Tale insieme permette al layer di imparare il tipo di funzione da implementare. \textbf{Questo paradigma include anche la correzione dell’errore.}
	\item\textbf{Apprendimento non supervisionato}, in cui vengono modificati i pesi del layer soltanto in base ai  sui dati in ingresso tentando di raggrupparli in classi e identificando cluster rappresentativi.
	\item \textbf{Apprendimento con rinforzo}, che costituisce una soluzione intermedia tra i primi due, è presente un  un agente esterno che guida il processo di apprendimento. \textbf{Lo scopo è quello di trovare una politica di scelta delle azioni che minimizzi il costo di esse sul lungo termine.}
\end{itemize}

Quando usiamo una rete neurale \textit{feed-forward} l’informazione $x$ scorre in una sola direzione dal layer di input attraverso tutti i layer nascosti per poi raggiungere quello di output producendo $f(x)$. Essa come già detto è l'approssimazione della funzione $f^*(x)$, perché questo avvenga si definisce la funzione di costo  $J(\theta)$ che computa la differenza media fra output f e quello vero f*, molto spesso si utilizza lo scarto quadratico medio. Il minimo di questa funzione approssimando $f \mbox{ a } f^*$ si ottiene variando i pesi e i bias della rete; la variazione è possibile invertendo lo scorrere dell'informazione grazie ad un algoritmo chiamato \textit{back-propagation}. 

\subsection{Gradiente}
La funzione di costo $J(\theta)$ dà un idea della qualità dell'output infatti per addestrare la rete dev'essere calcolato il minimo di questa funzione. Al fine di eseguire questa operazione spesso non si procede per via analitica ma per via matematica, calcolando il valore di $J(\theta)$ a vari intervalli e utilizzando formule di differenziazione come quella della derivata simmetrica. Questo calcolo, pur essendo molto veloce, produce un risultato approssimato che tuttavia si preferisce a quello analitico in quanto si predilige proprio la velocità di elaborazione a causa delle numerose ripetizioni dell'operazione nella fase si allenamento di una rete.  Facendo nuovamente il calcolo del gradiente e aggiornando i vari valori dei pesi e dei bias per ogni input, fino ad ottenere il minimo della funzione di costo, si addestra la rete. Questo algoritmo è chiamato discesa del gradiente o \textit{gradient descent}, il nuovo valore di $\theta$ si ottiene proseguendo nella direzione opposta del gradiente quindi di massima pendenza negativa. L'aggiornamento del parametro avviene in questo modo:
\begin{center}
	$\theta_{i+1}=\theta_i -\alpha\nabla J(\theta_i)$
\end{center}
il valore $\alpha$ è chiamato tasso di apprendimento e determina il $\Delta\theta=\theta_{i+1}-\theta_i$ con cui varia il parametro della funzione di costo. Maggiore è $\alpha$ maggiore è la velocità con cui si raggiunge il minimo ma anche maggiore è l'approssimazione di esso poiché, essendo calcolato matematicamente, dipende dagli step di calcolo di $J(\theta)$.




\section{Machine Learning} \label{sec:mlearning}
\begin{quoting}
\textit{A computer program is said to learn from experience E with respect to some class of tasks T and performance measure P if its performance at tasks in T, as measured by P, improves with experience E. }\footfullcite{Mitchell97}
\end{quoting}

\begin{flushright}
	\citeauthor{Mitchell97} in \citetitle{Mitchell97}
\end{flushright}

\noindent

\begin{itemize}
	\item Il compito T non è il processo di apprendimento stesso ma lo scopo per cui essa impara, esempi possono essere la classificazione degli oggetti o dei materiali, l’individuazione di un anomalia o la rimozione del rumore.
	\item  La misura della prestazione P serve per valutare la robustezza dell’algoritmo di \textit{machine learning}. Generalmente è specifico al tipo di compito come può essere l’accuratezza per la classificazione.
	\item L’esperienza E dipende dal tipo di classificazione cheviene effettuata, generalmente supervisionato o non supervisionato. 
\end{itemize}
In generale gli algoritmi di \textit{machine learning} sono tutti quegli algoritmi che permettono ad una macchina di imparare come può essere un semplice problema di regressione lineare.

\subsection{Deep Learning} \label{ssec:dlearning}
Il \textit{Deep Learning} è una sottoclasse degli algoritmi di \textit{Machine Learning}, i quali permettono di estrarre le \textit{features} connesse al problema autonomamente. Questo permette all'algoritmo di raggiungere una maggiore correttezza dei risultati, poiché le \textit{features }vengono "disegnate" direttamente dai dati e non da una rappresentazione di essi, a scapito però della maggior mole di dati da far analizzare per raggiungere tale accurtezza. Questo algoritmo usa una cascata di unità processionali non lineari per l’estrazione delle rappresentazioni, ogni livello equivale ad un’astrazione sempre maggiore e i concetti di basso livello servono a definire quelli di alto livello. Tipicamente si utilizzano delle reti neurali artificiali feedforward con una molteplicità di layer nascosti, molteplicità che dà la profondità e il nome alla rete.


\section{Conclusioni}
Grazie alla loro capacità di astrarre degli schemi generali dai dati possono risolvere problemi troppo complessi per essere svolti da altre tecniche computazionali.  Infatti le normali tecniche di machine learning richiedono una certa conoscenza pregressa sul problema di classificazione o apprendimento da risolvere. Le reti neurali invece apprendono la soluzione da esempi, affinando la loro capacità di risolvere il tipo di problema maggiore è il numero di esempi con i quali sono state allenate. Le due tecniche non sono in competizione bensì complementari, l'utilizzo di una o dell'altra dipende dal tipo di problema e dalla quantità di dati a disposizione per il training.
